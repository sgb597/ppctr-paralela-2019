#include <stdio.h>
#include <malloc.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <sys/times.h>
#include <omp.h>
#define APARTADO 1
//#define DEBUG

#define RAND rand() % 100

void init_mat_sup ();
void init_mat_inf ();
void init_mat ();
void matmulOMP ();
void matmul ();
void matmul_sup ();
void matmul_sup_guided ();
void matmul_sup_static ();
void matmul_sup_dynamic ();
void matmul_inf ();
void print_matrix (); // TODO

/* Usage: ./matmul <dim> [block_size]*/

int main (int argc, char* argv[])
{
	// int block_size = 1, dim = 5;
	// float *A, *B, *C;
	int dim = 1000;
	float *A, *B, *C;

	A = (float*) malloc((dim*dim)*sizeof(float));
	B = (float*) malloc((dim*dim)*sizeof(float));
	C = (float*) malloc((dim*dim)*sizeof(float));

	#if APARTADO == 1



	init_mat(A, dim);
	init_mat(B, dim);

	#ifdef DEBUG
	print_matrix(A, dim);
	print_matrix(B, dim);
	#endif
	//matmul secuencial
	double start3 = omp_get_wtime();

	matmul(A, B, C, dim);

	double end3 = omp_get_wtime() - start3;
	#ifdef DEBUG
	print_matrix(C, dim);
	#endif
	//matmul con OMP runtime
	double start = omp_get_wtime();

	matmulOMP(A, B, C, dim);

	double end = omp_get_wtime() - start;

	#ifdef DEBUG
	print_matrix(C, dim);
	#endif
	printf("\nSequential matrix multiplication took %lf seconds\n", end3);
	printf("\nParallel matrix multiplication took %lf seconds\n", end);
	#endif



	#if APARTADO == 2
	init_mat_sup(dim, A);
	init_mat_inf(dim, B);
	//Multiplicacion de matrices sup * inf secuencial
	double start1 = omp_get_wtime();

	matmul_sup (A, B, C, dim);

	double end1 = omp_get_wtime() - start1;

	//matmul_sup con OMP static
	double start2 = omp_get_wtime();

	matmul_sup_static (A, B, C, dim);

	double end2 = omp_get_wtime() - start2;

	print_matrix(A, dim);
	print_matrix(B, dim);
	print_matrix(C, dim);

	printf("\nUpper matrix multiplication took %lf seconds\n", end1);
	printf("\nParallel static upper matrix multiplication took %lf seconds\n", end2);
	#endif
	// //dim = atoi (argv[1]);
	// if (argc == 3) block_size = atoi (argv[2]);
	//

	exit (0);
}

void init_mat_sup (int dim, float *M)
{
	int i,j,m,n,k;

	for (i = 0; i < dim; i++) {
		for (j = 0; j < dim; j++) {
			if (j <= i)
			M[i*dim+j] = 0.0;
			else
			M[i*dim+j] = RAND;
		}
	}
}

void init_mat_inf (int dim, float *M)
{
	int i,j,m,n,k;

	for (i = 0; i < dim; i++) {
		for (j = 0; j < dim; j++) {
			if (j >= i)
			M[i*dim+j] = 0.0;
			else
			M[i*dim+j] = RAND;
		}
	}
}

void matmulOMP (float *A, float *B, float *C, int dim)
{
	int i, j, k;

	for (i=0; i < dim; i++)
		for (j=0; j < dim; j++)
			C[i*dim+j] = 0.0;

	omp_set_num_threads(2);

	#pragma omp parallel default (shared) private(i, j, k)
	{
		//printf("Hay %d threads en la region paralela segun el thread %d\n", omp_get_num_threads(), omp_get_thread_num());
		#pragma omp for schedule(runtime)
			for (i=0; i < dim; i++)
				for (j=0; j < dim; j++)
					for (k=0; k < dim; k++)
						C[i*dim+j] += A[i*dim+k] * B[j+k*dim];
	}
}

void matmul (float *A, float *B, float *C, int dim)
{
	int i, j, k;

	for (i=0; i < dim; i++)
		for (j=0; j < dim; j++)
			C[i*dim+j] = 0.0;

			for (i=0; i < dim; i++)
				for (j=0; j < dim; j++)
					for (k=0; k < dim; k++)
						C[i*dim+j] += A[i*dim+k] * B[j+k*dim];
}

void matmul_sup_static(float *A, float *B, float *C, int dim)
{
	int i, j, k;

	for (i=0; i < dim; i++)
	for (j=0; j < dim; j++)
	C[i*dim+j] = 0.0;

	#pragma omp parallel default(shared) private(i, j, k)
	{
		#pragma omp for schedule(static)
			for (i=0; i < (dim-1); i++)
				for (j=0; j < (dim-1); j++)
					for (k=(i+1); k < dim; k++)
						C[i*dim+j] += A[i*dim+k] * B[j+k*dim];
	}
}

void matmul_sup_guided(float *A, float *B, float *C, int dim)
{
	int i, j, k;

	for (i=0; i < dim; i++)
	for (j=0; j < dim; j++)
	C[i*dim+j] = 0.0;

	#pragma omp parallel default(shared) private(i, j, k)
	{
		#pragma omp for schedule(guided)
			for (i=0; i < (dim-1); i++)
				for (j=0; j < (dim-1); j++)
					for (k=(i+1); k < dim; k++)
						C[i*dim+j] += A[i*dim+k] * B[j+k*dim];
	}
}

void matmul_sup_dynamic(float *A, float *B, float *C, int dim)
{
	int i, j, k;

	for (i=0; i < dim; i++)
	for (j=0; j < dim; j++)
	C[i*dim+j] = 0.0;

	#pragma omp parallel default(shared) private(i, j, k)
	{
		#pragma omp for schedule(dynamic)
			for (i=0; i < (dim-1); i++)
				for (j=0; j < (dim-1); j++)
					for (k=(i+1); k < dim; k++)
						C[i*dim+j] += A[i*dim+k] * B[j+k*dim];
	}
}

void matmul_sup(float *A, float *B, float *C, int dim)
{
	int i, j, k;

	for (i=0; i < dim; i++)
	for (j=0; j < dim; j++)
	C[i*dim+j] = 0.0;

			for (i=0; i < (dim-1); i++)
				for (j=0; j < (dim-1); j++)
					for (k=(i+1); k < dim; k++)
						C[i*dim+j] += A[i*dim+k] * B[j+k*dim];
}

void matmul_inf (float *A, float *B, float *C, int dim)
{
	int i, j, k;

	for (i=0; i < dim; i++)
	for (j=0; j < dim; j++)
	C[i*dim+j] = 0.0;

	for (i=1; i < dim; i++)
	for (j=1; j < dim; j++)
	for (k=0; k < i; k++)
	C[i*dim+j] += A[i*dim+k] * B[j+k*dim];
}

void print_matrix (float *M, int dim)
{
	int i, j;
	printf("\n");
	for(i = 0; i < dim; i++)
	{
		for(j = 0; j < dim; j++)
		{
			printf("%lf\t", M[i * dim + j]);
		}
		printf("\n");
	}
}

void init_mat (float *M, int dim)
{
	int i, j;

	for(i = 0; i < dim; i++)
	{
		for(j = 0; j < dim; j++)
		{
			M[i * dim + j] = RAND;
		}
	}
}
