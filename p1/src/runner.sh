#!/usr/bin/env bash
file=results1_opt.log
touch $file
for season in 1 2; do
    for benchcase in st1 mt1 st2 mt2; do
        echo $benchcase >> $file
        for i in `seq 1 1 10`;
        do
            printf "\n$i:" >> $file # iteration
            ./benchsuite.sh $benchcase >> $file # results dumped
            sleep 1
        done
    done
done
