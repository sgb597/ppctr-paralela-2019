#include<iostream>
#include<string.h>
#include<thread>
#include<mutex>
#include<list>
#include<condition_variable>
#include<time.h>
#include<atomic>
#include<sys/time.h>

//FEATURE_OPTIMIZE 0 = deshabilitado
#define FEATURE_OPTIMIZE 0
//#define FEATURE_LOGGER
//#define DEBUG

double resultado = 0;
bool op_extra = true;

#ifdef FEATURE_LOGGER
bool operar = false;
double logger_result = 0;
std::mutex mtx;
std::mutex mtx_logger;
std::condition_variable cv;
std::list <double> logger_list;
void logger(int numThreads, char ops[], double *registro);
#endif

#if FEATURE_OPTIMIZE == 1
std::atomic<double> multiThreadedSum(0);
#endif

void arrOperation(int posInicial, int trozo, int arraySize, int numThreads, double arr[], char ops[], double *registro);
void arrOperationOptimized(int posInicial, int trozo, int arraySize, int numThreads, double arr[], char ops[]);

int main(int argc, char* argv[]){

  if(argc == 1){
    printf("Error!! Size of array and operator must be specified\n");
    exit(-1);
  }

  //random number seed
  srand(time(NULL));

  //Se guarda el tipo de operacion que se quiere realizar (sum, sub o xor)
  char ops[3]={0x0};
  strcpy(ops, argv[2]);


  size_t arraySize = atol(argv[1]), i;

  if(arraySize <= 0){
    printf("Error!! Invalid iterations\n");
    exit(-2);
  }

  double *arr = (double*) malloc(arraySize * sizeof(double));

  //array a operar se llena de elementos
  for(i = 0; i < arraySize; i++){
    arr[i] = rand() % 100 + 1;
    #ifdef DEBUG
    arr[i] = i; //modelo debug de array para arraySize
    printf("%lf\n", arr[i]);
    #endif
  }

  //codigo secuencial
  if(argc == 3){

    struct timeval tv1, tv2;
    gettimeofday(&tv1, NULL);

    //suma de todos los elementos de mi array
    if(strcmp(ops, "sum") == 0 || strcmp(ops, "Sum") == 0){
      for(i = 0; i < arraySize; i++){
        resultado = resultado + arr[i];
        #ifdef DEBUG
        printf("%lf\n", resultado);
        #endif
      }
    }

    //resta de todos los elementos
    else if(strcmp(ops, "sub") == 0 || strcmp(ops, "Sub") == 0){
      for(i = 0; i < arraySize; i++){
        resultado = resultado - arr[i];
        #ifdef DEBUG
        printf("%lf\n", resultado);
        #endif
      }
    }

    //xor de los elementos
    else if(strcmp(ops, "xor") == 0 || strcmp(ops, "Xor") == 0){
      for(i = 0; i < arraySize; i++){
        resultado = int(resultado)^int(arr[i]);
        #ifdef DEBUG
        printf("%lf\n", resultado);
        #endif
      }
    }
    gettimeofday(&tv2, NULL);
    printf ("Total time = %f seconds and the result is %lf\n",
    ((double) (tv2.tv_usec - tv1.tv_usec) / 1000000 +
    (double) (tv2.tv_sec - tv1.tv_sec)), resultado);
  }
  else{

    int numThreads = atoi(argv[3]);
    int trozo = arraySize/numThreads;
    double registro[numThreads];

    #ifdef FEATURE_LOGGER
    std::thread thread_logger(logger, numThreads,  ops, &(*registro));
    #endif

    std::thread threads[numThreads];

    //---------------------------------------------------------
    //CODIGO CON FEATURE_OPTIMIZE (suma a una variable atomica)
    #if FEATURE_OPTIMIZE == 1

    struct timeval tv1, tv2;
    gettimeofday(&tv1, NULL);

        for(i=0; i<numThreads; i++){
          threads[i] = std::thread(arrOperationOptimized, i*trozo, trozo, arraySize, numThreads, arr, ops);
        }

        for(i=0; i<numThreads; i++){
          threads[i].join();
        }
        gettimeofday(&tv2, NULL);
        printf ("Total time = %f seconds and the result is ",
        (double) (tv2.tv_usec - tv1.tv_usec) / 1000000 +
        (double) (tv2.tv_sec - tv1.tv_sec));
        std::cout <<  multiThreadedSum.load() << "\n";
    #endif

    //------------------------------------------
    //CODIGO SIN FEATURE_OPTIMIZE

    #if FEATURE_OPTIMIZE == 0

    struct timeval tv1, tv2;
    gettimeofday(&tv1, NULL);

    for(i=0; i<numThreads; i++){
      threads[i] = std::thread(arrOperation, i*trozo, trozo, arraySize, numThreads, arr, ops, &registro[i]);
    }

    for(i=0; i<numThreads; i++){
      threads[i].join();
      resultado += registro[i];
      #ifdef DEBUG
      printf("%lf\n", resultado);
      #endif
    }
    gettimeofday(&tv2, NULL);
    printf ("Total time = %f seconds and the result is %lf\n",
    ((double) (tv2.tv_usec - tv1.tv_usec) / 1000000 +
    (double) (tv2.tv_sec - tv1.tv_sec)), resultado);

    #ifdef FEATURE_LOGGER
    thread_logger.join();

    printf("El valor del logger es: %lf\n", logger_result);

    if(resultado == logger_result){
      printf("Logger y threads han obtenido el mismo valor\n");
      exit(0);
    }
    else{
      exit(1);
    }
    #endif
  #endif
  }

  free(arr);
  return 0;
}

void arrOperation(int posInicial, int trozo, int arraySize, int numThreads, double arr[], char ops[], double *registro){
  int posFinal = posInicial+trozo, i = 0;

  *registro = 0;

  if(strcmp(ops, "sum") == 0 || strcmp(ops, "Sum") == 0){
    for(i = posInicial; i < posFinal; i++){
      *registro = *registro + arr[i];
    }
  }

  else if(strcmp(ops, "sub") == 0 || strcmp(ops, "Sub") == 0){
    for(i = posInicial; i < posFinal; i++){
      *registro = *registro - arr[i];
    }
  }

  else if(strcmp(ops, "xor") == 0 || strcmp(ops, "Xor") == 0){
    for(i = posInicial; i < posFinal; i++){
      *registro = int(*registro)^int(arr[i]);
    }
  }

  if(op_extra && arraySize % numThreads != 0){

    op_extra = false;

    if(strcmp(ops, "sum") == 0 || strcmp(ops, "Sum") == 0){
      for(i = numThreads*trozo; i < arraySize; i++){
        *registro = *registro + arr[i];
      }
    }

    else if(strcmp(ops, "sub") == 0 || strcmp(ops, "Sub") == 0){
      for(i = numThreads*trozo; i < arraySize; i++){
        *registro = *registro - arr[i];
      }
    }

    else if(strcmp(ops, "xor") == 0 || strcmp(ops, "Xor") == 0){
      for(i = numThreads*trozo; i < arraySize; i++){
        *registro = int(*registro)^int(arr[i]);
      }
    }
  }
  #ifdef FEATURE_LOGGER
  operar = true;  //variable booleana global inicializada en false
  logger_list.push_back(*registro);
  cv.notify_one();
  #endif
}

#ifdef FEATURE_LOGGER
void logger(int numThreads, char ops[], double *registro){
  //sthreads = cuantos threads se han procesado
  int sthreads = 0, i;
  std::unique_lock<std::mutex> lck(mtx_logger);

  while(sthreads < numThreads){

    cv.wait(lck, [] {return (operar) ? true : false; });
    double thread_item = logger_list.front();

    if(strcmp(ops, "sum") == 0 || strcmp(ops, "Sum") == 0){
      mtx.lock();
      logger_result = logger_result + thread_item;
      mtx.unlock();
    }

    else if(strcmp(ops, "sub") == 0 || strcmp(ops, "Sub") == 0){
      mtx.lock();
      logger_result = logger_result - thread_item;
      mtx.unlock();
    }

    else if(strcmp(ops, "xor") == 0 || strcmp(ops, "Xor") == 0){
      mtx.lock();
      logger_result = int(logger_result)^int(thread_item);
      mtx.unlock();
    }

    logger_list.pop_front();
    sthreads++;
    if(logger_list.empty()){operar = false;}
  }
  for(i = 0; i < numThreads; i++){
    printf("\nThread %d result: %f\n", i, registro[i]);
  }
}
#endif

#if FEATURE_OPTIMIZE == 1

void arrOperationOptimized(int posInicial, int trozo, int arraySize, int numThreads, double arr[], char ops[]){
  int posFinal = posInicial+trozo, i = 0;

  if(strcmp(ops, "sum") == 0 || strcmp(ops, "Sum") == 0){
    for(i = posInicial; i < posFinal; i++){
      multiThreadedSum = multiThreadedSum + arr[i];
    }
  }

  else if(strcmp(ops, "sub") == 0 || strcmp(ops, "Sub") == 0){
    for(i = posInicial; i < posFinal; i++){
      multiThreadedSum = multiThreadedSum - arr[i];
    }
  }

  else if(strcmp(ops, "xor") == 0 || strcmp(ops, "Xor") == 0){
    for(i = posInicial; i < posFinal; i++){
      multiThreadedSum = int(multiThreadedSum)^int(arr[i]);
    }
  }

  if(op_extra && arraySize % numThreads != 0){

    op_extra = false;

    if(strcmp(ops, "sum") == 0 || strcmp(ops, "Sum") == 0){
      for(i = numThreads*trozo; i < arraySize; i++){
        multiThreadedSum = multiThreadedSum + arr[i];
      }
    }

    else if(strcmp(ops, "sub") == 0 || strcmp(ops, "Sub") == 0){
      for(i = numThreads*trozo; i < arraySize; i++){
        multiThreadedSum = multiThreadedSum - arr[i];
      }
    }

    else if(strcmp(ops, "xor") == 0 || strcmp(ops, "Xor") == 0){
      for(i = numThreads*trozo; i < arraySize; i++){
        multiThreadedSum = int(multiThreadedSum)^int(arr[i]);
      }
    }
  }
}
#endif
