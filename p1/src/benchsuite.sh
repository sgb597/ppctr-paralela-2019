#!/usr/bin/env bash
size=100000
size2=500670000
op=xor
nthreads=4
case "$1" in
    st1)
        ./p1 $size $op
        ;;
    mt1)
        ./p1 $size $op $nthreads
        ;;
    st2)
        ./p1 $size2 $op
        ;;
    mt2)
        ./p1 $size2 $op $nthreads
        ;;
esac
