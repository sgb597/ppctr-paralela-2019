# P1: Multithreading en C++
Sebastián García Bustamante

Fecha 11/12/2019

## Prefacio

En esta práctica se estudiaron e implementaron los conceptos de threads de C++, mutex, variables condicionales, variables atómicas, listas de C++, variables de preprocesador, bash scripting, benchmarking.

La práctica me ha parecido útil para poder ver lo complejo que puede llegar a hacer la paralelización de código sin la implementación de OpenMP. OpenMP facilita bastante el trabajo de paralelización, pero también le quita cierto grado de control al programador. Esta práctica me ha parecido un desafío interesante ya que al empezar la clase de Programación Paralela sabía muy poco sobre los conceptos previamente especificados y he notado una gran mejora en cuanto a mi programación gracias a este desafío.

## Índice

1. Sistema
2. Diseño e Implementación del Software
3. Metodología y desarrollo de las pruebas realizadas
4. Discusión
5. Propuestas optativas

## 1. Sistema

Descripción del sistema donde se realiza el *benchmarking*.


- Memoria de 8GB
- Intel® Core™ i5-7200U CPU @ 2.50GHz
- CPU 4 cores: 1 socket, 2 threads/core, 2 cores/socket.
- Sistema Operativo: Ubuntu 18.04.3 LTS 64-bit
- Intel® HD Graphics 620 (Kaby Lake GT2)
- CPU @3089.305 MHz (fijada con cpufreq)
- Trabajo y benchmarks sobre HDD
- 300 procesos en promedio antes de ejecuciones
- flags: fpu, vme, de, pse, tsc, msr, pae, mce, cx8, apic, sep, mtrr, pge, mca, cmov,  sse, sse2, ss y avx

## 2. Diseño e Implementación del Software

### Base

El objetivo de esta parte de la práctica fue desarrollar una CLI que acepta el número de elementos de un array y una operación a efectuar (sum, sub o xor). Las operaciones de esta CLI también se pueden ejecutar de manera paralela al indicar un tercer parámetro adicional que vendría siendo el número de threads que ejecutarán la operación indicada. Con estos parámetros el programa llenará un array dinámicamente con el tamaño indicado por el usuario previamente y los números que componen el array serán números aleatorios del 0 al 100. Una vez se ha llenado se efectuará la operación que haya sido indicada por el usuario con los números que componen al array.

El orden que se utiliza para reconocer estos argumentos es el siguiente:
- Tamaño de array como segundo argumento (el primero siendo el nombre del programa).
- Operación como tercero.
- Número de threads como cuarto.

Para reconocer los argumentos que el usuario introduzca el código utiliza un array de caracteres llamado "ops" en el cual se guarda la operación que el usuario desea realizar, un array llamado "arr" que se llena con la cantidad de números aleatorios que el usuario indique. Para la versión paralela del programa se utilizan los threads de C++ para llevar a cabo la operación que el usuario indique. El número de threads que el usuario indique como su tercer parámetro se guarda una variable numThreads.

Para la versión secuencial se utilizan if statements y nested if statements para operar sobre el array dependiendo de que haya pasado como argumento el usuario. Luego para la versión paralela (en caso que el usuario especifique número de threads) se utiliza la misma lógica que el programa secuencial, pero se encuentra dentro de una función llamada "arrOperation" que se le pasa a los threads. la función arrOperation utiliza el modulo entre el array y el número de threads para saber si habrá un "chunk" o trozo que sobre para que se lo lleve el primer thread en terminar.

El modo DEBUG se ha implementado a través de la compilación condicional con el fin de proporcionar al profesorado y al desarrollador una visión simplificada de como funciona el programa. Ha sido una herramienta clave para poder probar funcionamiento correcto del programa. Un ejemplo claro de la utilización del modo DEBUG es la creación de un array pequeño de números del cual ya se sabe la suma, resta y xor con antemano para asegurar que los resultados de todas las operaciones están funcionando como deberían. Adicionalmente, este modo también habilita impresiones dentro de la ROI (lo cual no se utiliza en ningún otro modo) para poder analizar lo que ocurre dentro de las funciones utilizadas.

### FEATURE_LOGGER

El modo FEATURE_LOGGER tiene como objetivo la creación del thread logger que obtiene los resultados de cada hilo y los opera él mismo. La activación de este penaliza el rendimiento pero permite obtener los resultados de las operaciones. Para el thread logger se ha creado otra función que tenga las mismas operaciones que la función arrOperation pero funciona a base de notify y wait para poder ir sumando las operaciones que hacen los threads. Cuando un thread finaliza su operación cambia una variable global llamada "operar", añade su resultado al final de una lista de C++ y notifica al logger para que realice la operación. Cuando el logger tiene el unique lock "lck" y la variable global "operar" es true procede a sumar los resultados de los threads. El logger guarda el primer valor de la lista en una variable local y opera eso con su variable global "logger_result". Al terminar de operar, el logger quita el primer valor (con el cual acaba de operar) de la lista de resultados, suma uno a la variable de "sthreads" para indicar que se ha procesado el resultado de un thread, cambia el valor de operar a false indicando que la lista esta vacía y muestra los resultados con un loop.

### FEATURE_OPTIMIZE

El objetivo de este apartado de la práctica ha sido generar una versión optimizada del código original. Para optimizar el código he utilizado variables atómicas de C++. La implementación de una variable atómica sobre la cual se guardan los resultados de los threads permite solucionar el problema de la condición de carrera que ocurre en esta práctica. Si la operación no se puede realizar en un "clock tick" el compilador coloca thread guards antes y después de la variable para que no el valor de esta variable no se pueda cambiar. La estructura que se utiliza para el código multihtread sigue siendo igual solo que se utiliza la variable global y atómica llamada "multiThreadedSum" para guardar las operaciones de los threads.

### Conector con Java

El objetivo de este apartado ha consistido de modularizar el código a modo de librería de forma que se pueda ejecutar el código Java con la porción extraída de C. Para llevar esto a cabo e ha utilizado la interfaz proporcionada por el profesor para ejecutar C desde Java. El código del apartado base se ha introducido dentro de la función todo() del archivo P1Bridge de C++. A la función todo() se le introducen los mismos tres argumentos que se introducían a la CLI del apartado Base. Al final del archivo P1Bridge de C++ se encuentra la siguiente línea que permite conectar nuestro código de C++ con Java para que pueda ser ejecutado una vez ha sido modularizado a modo de librería.

```C
JNIEXPORT int JNICALL Java_P1Bridge_compute(JNIEnv *env, jobject thisObj, int arraySize, int ops, int numThreads) {
todo(arraySize, ops, numThreads);
return 0;
}
```

## 3. Metodología y Desarrollo de las Pruebas Realizadas

### Proceso de Benchmarking

Para las pruebas de benchmarking de mi código se han utilizado los siguientes scripts (modificados de lo que se proporciona en clase). Con esto se ha probado el código Base y también el código optimizado tal y como se pide. Los tamaños seleccionados para el array vienen de pruebas que se han llevado a cabo anteriormente.

La frecuencia de la CPU se ha fijado con cpufreq. Para averiguar la frecuencia máxima de mi CPU he utilizado lscpu, la cual me indica que la frecuencia máxima es @3100 MHz y luego con el comando top he podido identificar la cantidad de tareas que se estaban corriendo en mi sistema. El número de tareas indicado por el comando top era en promedio 300. Lo único abierto en el ordenador a la hora de ejecutar el runner.sh es la terminal desde la cual se está ejecutando. Debido a que he realizado los benchmarks en un portátil, este se encuentra enchufado a una toma de corriente para que no esté corriendo de batería.

Para obtener los resultados del tiempo de ejecución se ha utilizado la API gettimeofday. La manera en la que se ha implementado es la siguiente:

```C
struct timeval tv1, tv2;
gettimeofday(&tv1, NULL);
//Código a ejecutar
gettimeofday(&tv2, NULL);
printf ("Total time = %f seconds and the result is %lf\n",
((double) (tv2.tv_usec - tv1.tv_usec) / 1000000 +
(double) (tv2.tv_sec - tv1.tv_sec)), resultado);
```
La variable resultado es el resultado de las operaciones realizadas por el código paralelo o secuencial.


Para realizar los benchmarks se ha utilizado una modificación del código proporcionado por el profesor que se encuentra a continuación.

**runner.sh:**

```sh
#!/usr/bin/env bash
file=results1.log
touch $file
for season in 1 2; do
    for benchcase in st1 mt1 st2 mt2; do
        echo $benchcase >> $file
        for i in `seq 1 1 10`;
        do
            printf "\n$i:" >> $file # iteration
            ./benchsuite.sh $benchcase >> $file # results dumped
            sleep 1
        done
    done
done
```

**benchsuite.sh:**

```sh
#!/usr/bin/env bash
size=100000
size2=500670000
op=xor
nthreads=4
case "$1" in
    st1)
        ./p1 $size $op
        ;;
    mt1)
        ./p1 $size $op $nthreads
        ;;
    st2)
        ./p1 $size2 $op
        ;;
    mt2)
        ./p1 $size2 $op $nthreads
        ;;
esac
```

Para la medición de tiempos de ejecución del conector de Java no se ha utilizado el script anterior debido a dificultades. Por lo tanto, se ha ejecutado desde la consola con el comando "make run" de la Makefile que se encuentra en esa carpeta. Se han modificado los argumentos que se pasan en el run para que el conector con Java ejecute los mismos argumentos que cuando se midieron tiempos de ejecución con el script. Se han obtenido dos "seasons" con 10 ejecuciones para el conector de Java.

### Captura de Resultados

Los resultados se encuentran en su totalidad en los ficheros .log de la carpeta "src" de la Práctica  1. En estos resultados se pueden ver los cambios en rendimiento dependiendo del tamaño de array y del tipo de código que se utilice. Los tamaños de array se han escogido después de haber probado con distintos tamaños para el array a procesar. Para mi maquina estos tamaños muestran un rango significativo debido a que rangos superiores hacen que la ejecución del programa en mi máquina actue de forma impredecible. Este efecto se ha estudiado y se han implementado medidas que han mejorado el rango original que era menor. Para esto se ha utilizado el tipo "size_t" a la hora inicializar "arraySize" y declarar "i".

### Generación de Gráficas

#### Gráficas del Apartado Base
Las gráficas de rendimiento se han generado utilizando Microsoft Excel 2016. ST (single thread) es utilizado para referirse a una ejecución secuencial y MT (multiple threads) para referirse a las ejecuciones paralelas.
![](src/images/Base_small.png)

Tiempo de ejecución para la operación xor de 100000 elementos. Se usaron 4 threads para la versión paralela. El eje horizontal contiene las series de ejecuciones que se han realizado y el eje vertical el tiempo que ha tomado.


![](src/images/Base_grande.png)

Tiempo de ejecución para la operación xor de 500670000 elementos utilizando 4 threads para MT. El eje horizontal contiene las series de ejecuciones que se han realizado y el eje vertical el tiempo que ha tomado.

Las gráficas muestran visualmente como se consigue una mejora de rendimiento a la hora de aplicar el multithreading a la versión secuencial del programa. La mejora se vuelve más notable cuando se utilizan tamaños de array grandes. La tabla a continuación muestra un análisis de las medias (segundos) de los resultados y la desviación estándar como medida de dispersión.

|           | ST Base Array Pequeño | MT Base Array Pequeño | ST Base Array Grande | MT Base Array Grande |
|-----------|-----------------------|-----------------------|----------------------|----------------------|
|  Media    | 5.98E-04              |  4.68E-04             | 2.98                 | 1.44                 |
| Std. Dev. | 2.12E-05              | 8.35E-05              | 6.37E-03             | 0.25                 |

| Base       |  Array 100000 | Array  500670000 |
|------------|---------------|------------------|
| Speedup    | 1.2778        | 2.0694           |
| Eficiencia | 0.3194        | 0.5173           |

#### Gráficas para FEATURE_OPTIMIZE

![](src/images/Atomic_small.png)

Tiempo de ejecución para la operación xor de 100000 elementos. Se usaron 4 threads para la versión paralela. El eje horizontal contiene las series de ejecuciones que se han realizado y el eje vertical el tiempo que ha tomado.

![](src/images/atomic_grande.png)

Tiempo de ejecución para la operación xor de 500670000 elementos utilizando 4 threads para MT. El eje horizontal contiene las series de ejecuciones que se han realizado y el eje vertical el tiempo que ha tomado.

|           | ST Atómico Array Pequeño | MT Atómico Array Pequeño | ST Atómico Array Grande | MT Atómico Array Grande |
|-----------|--------------------------|--------------------------|-------------------------|-------------------------|
|  Media    | 5.97E-04                 |  2.61E-03                | 2.98                    | 11.89                   |
| Std. Dev. | 2.22E-05                 | 2.23E-04                 | 6.83E-03                | 0.31                    |

| Optimize   |  Array 100000 | Array  500670000 |
|------------|---------------|------------------|
| Speedup    | 0.2287        | 0.2506           |
| Eficiencia | 0.0571        | 0.0626           |

Las gráficas y la tabla muestran un caso diferente para la implementación de atómicos en esta práctica. La razón porque se vé tanto overhead a la hora de utilizar variables atómicas en el código es debido a la naturaleza de las variables atómicas. Para cambiar una variable atómica se necesita realizar la operación en un cambio de reloj y si esto no puede ser garantizado no ocurre la operación hasta cuando se pueda realizar de esta manera. Por otra parte, la manera en la que se ha desarrollado el apartado Base, se ahorran overheads innecesarios debido a que se guardan los resultados dentro de un array en el cual cada thread tiene su propio índice. Al guardar los resultados de las operaciones dentro de un array no hay condición de carrera debido a que cada hilo lo guardará en un índice del array y por lo tanto no se necesita proteger con mutex o variables atómicas que añaden overhead innecesario.

El overhead que surge al implementar variables atómicas se puede ver claramente en la tabla y en la gráfica cuando se procesan arrays grandes. La media del tiempo de ejecución para el FEATURE_OPTIMIZE es casi 12 segundos y para el código secuencial es de 3 segundos. La desviación estándar de estos datos es de 0.00683 para mi código secuencial y 0.31 para el FEATURE_OPTIMIZE, lo cual muestra que mis tiempos de ejecución para este están mucho más dispersos que para el código secuencial.

#### Gráficas para Conector con Java

![](src/images/java_small.png)

Tiempo de ejecución para la operación xor de 100000 elementos. Se usaron 4 threads para el conector de Java. El eje horizontal contiene las series de ejecuciones que se han realizado y el eje vertical el tiempo que ha tomado.

![](src/images/java_grande.png)

Tiempo de ejecución para la operación xor de 500670000 elementos utilizando 4 threads para el conector de Java. El eje horizontal contiene las series de ejecuciones que se han realizado y el eje vertical el tiempo que ha tomado.

|           | MT Java Conector Array Pequeño | MT Java Conector Array Grande |
|-----------|--------------------------------|-------------------------------|
|  Media    | 5.57E-04                       |   1.6510                      |
| Std. Dev. | 6.93E-05                       |   0.2943                      |

| Java       |  Array 100000 | Array  500670000 |
|------------|---------------|------------------|
| Speedup    | 1.0715        | 1.8049           |
| Eficiencia | 0.2678        | 0.4512           |

El Conector de Java ha tomado más tiempo en promedio que el apartado Base como es de esperarse ya que toma más tiempo ejecutar el código desde Java con la porción extraída de C. Al analizar los resultados en la tabla se puede otro patrón común que es el incremento en la desviación estándar al aumentar el tamaño del array. Al incrementar el tamaño del array se aumentan los tiempos de ejecución y también la variación de los tiempos de ejecución obtenidos.

## 4. Discusión

En mi opinión se han cumplido los objetivos de esta práctica. Se ha realizado paralelización de un programa mediante el paradigma de programación memoria compartida ya que se ha desarrollado un programa en el cual múltiples threads realizan operaciones y estos comparten memoria de los cuales se leen valores y se escriben los resultados. Esto se ha realizado mediante las primitivas de C++ que hemos visto en clase. Adicionalmente, se ha se han implementado los mecanismos de paralelismo de datos y paralelismo de tareas a la hora de tener threads que operan diferentes regiones del mismo array y en casos como el FEATURE_LOGGER que va también en paralelo con los demás hilos realizando paralelismo de tareas con los datos obtenidos por los otros threads.

La programación paralela en C++ nos permite tener un gran control y modificar las operaciones a realizar con los threads de C++. Este nivel de control no es posible de obtener con la API OpenMP ya que tiene un cierto nivel de abstracción que permite simplificar mucho el proceso de paralelización pero a la vez le quita al programador cierto grado de control sobre los procesos que están ocurriendo por debajo.
