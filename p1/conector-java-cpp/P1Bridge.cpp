#include<iostream>
#include<string.h>
#include<thread>
#include<time.h>
#include<sys/time.h>
#include <jni.h>
#include "P1Bridge.h"

#define DEBUG 0

double resultado = 0;
bool op_extra = true;

void arrOperation(int posInicial, int trozo, int arraySize, int numThreads, double arr[], int ops, double *registro);

int todo (int arraySize,  int ops, int numThreads) {


  srand(time(NULL));

  int trozo = arraySize/numThreads, i;
  double registro[numThreads];
  double *arr = (double*) malloc(arraySize * sizeof(double));

  //array a operar se llena de elementos
  for(i = 0; i < arraySize; i++){
    #if DEBUG == 0
    arr[i] = rand() % 100 + 1;
    #endif
    #if DEBUG == 1
    arr[i] = i;
    #endif
  }

  std::thread threads[numThreads];

  struct timeval tv1, tv2;
  gettimeofday(&tv1, NULL);

  for(i=0; i<numThreads; i++){
    threads[i] = std::thread(arrOperation, i*trozo, trozo, arraySize, numThreads, arr, ops, &registro[i]);
  }

  for(i=0; i<numThreads; i++){
    threads[i].join();
    resultado += registro[i];
    #if DEBUG == 1
    printf("%lf\n", resultado);
    #endif
  }
  gettimeofday(&tv2, NULL);
  printf ("Total time = %f seconds and the result is %lf\n",
  ((double) (tv2.tv_usec - tv1.tv_usec) / 1000000 +
  (double) (tv2.tv_sec - tv1.tv_sec)), resultado);

  free(arr);
  return resultado;
}

void arrOperation(int posInicial, int trozo, int arraySize, int numThreads, double arr[], int ops, double *registro){
  int posFinal = posInicial+trozo, i = 0;

  *registro = 0;

  if(ops == 1){
    for(i = posInicial; i < posFinal; i++){
      *registro = *registro + arr[i];
    }
  }

  else if(ops == 2){
    for(i = posInicial; i < posFinal; i++){
      *registro = *registro - arr[i];
    }
  }

  else if(ops == 3){
    for(i = posInicial; i < posFinal; i++){
      *registro = int(*registro)^int(arr[i]);
    }
  }

  if(op_extra && arraySize % numThreads != 0){

    op_extra = false;

    if(ops == 1){
      for(i = numThreads*trozo; i < arraySize; i++){
        *registro = *registro + arr[i];
      }
    }

    else if(ops == 2){
      for(i = numThreads*trozo; i < arraySize; i++){
        *registro = *registro - arr[i];
      }
    }

    else if(ops == 3){
      for(i = numThreads*trozo; i < arraySize; i++){
        *registro = int(*registro)^int(arr[i]);
      }
    }
  }
}



JNIEXPORT int JNICALL Java_P1Bridge_compute(JNIEnv *env, jobject thisObj, int arraySize, int ops, int numThreads) {
  todo(arraySize, ops, numThreads);
  return 0;
}
