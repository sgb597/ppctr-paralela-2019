public class P1 {
   public static void main(String[] args) {
    int arraySize = Integer.parseInt(args[0]);
     int ops = Integer.parseInt(args[1]);
     int numThreads = Integer.parseInt(args[2]);
     P1Bridge p = new P1Bridge();
     int resultado = p.compute(arraySize, ops, numThreads);
     System.out.println("Resultado: "+resultado);
   }
}
