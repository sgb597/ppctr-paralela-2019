# P2: Optimizing
Sebastián García Bustamante

Fecha 30/11/2019

## Prefacio

En esta práctica se estudiaron e implementaron los conceptos de threads de C++, OpenMP, variables de preprocesador, optimización de código y benchmarking.

La práctica me ha parecido un interesante desafío ya que requiere pensamiento crítico y matemática para poder optimizar el código secuencial original. Me ha llevado trabajo estudiar la función del código y luego optimizar las operaciones matemáticas fundamentales.

## Índice

1. Sistema
2. Diseño e Implementación del Software
3. Metodología y desarrollo de las pruebas realizadas
4. Discusión

## 1. Sistema

Descripción del sistema donde se realiza el *benchmarking*.


- Memoria de 8GB
- Intel® Core™ i5-7200U CPU @ 2.50GHz
- CPU 4 cores: 1 socket, 2 threads/core, 2 cores/socket.
- Sistema Operativo: Ubuntu 18.04.3 LTS 64-bit
- Intel® HD Graphics 620 (Kaby Lake GT2)
- CPU @3089.305 MHz (fijada con cpufreq)
- Trabajo y benchmarks sobre HDD
- 300 procesos en promedio antes de ejecuciones
- flags: fpu, vme, de, pse, tsc, msr, pae, mce, cx8, apic, sep, mtrr, pge, mca, cmov,   sse, sse2, ss y avx

## 2. Diseño e Implementación del Software

### Base

El objetivo de este apartado ha sido la optimización del código proporcionado. Para el primer apartado "Base" y los demás apartados se ha cambiado por completo el código que originalmente fue proporcionado. Esto ha ocurrido después de llevar a cabo un análisis sobre el funcionamiento del código. La única función que aportaba algo de valor era la función `sequential` (y aun así tenía varios aspectos que podían ser mejorados) que contiene las operaciones matemáticas necesarias para obtener el valor final de "ellipse". El struct `Aggregator` no aportaba nada de valor ya que lo único de valor que contenía era el campo `separator` que guardaba era (1/número de iteraciones) y algo similar ocurría con el struct `Data` que en realidad su único campo de valor era el número de iteraciones. Luego la función `parse` asignaba valores a cosas dentro de la estructura `Data` que eran completamente redundantes e innecesarios. `Sequential` contenía las operaciones matemáticas necesarias para obtener el valor deseado de ellipse pero utilizaba maneras muy ineficientes de llevar a cabo estos cálculos ya que tenía que acceder a campos de los structs previamente descritos por ninguna razón aparte de hacer mas ineficiente el código. Por lo tanto, fue necesario llevar a cabo una simplificación de estas operaciones matemáticas. Primero elimine todos los structs pero asegurando que los números de los campos `A` y `B` del `Aggregator` quedaran en sequential. Segundo simplifique las operaciones matemáticas a través de ir iterando con pocos números por la ecuación para "x" y luego la de tmp.  

```C
Aggregator* agg = (Aggregator*)malloc(sizeof(Aggregator));
agg->separator = (double)1/(double)iterations;
agg->A = 1.5;
agg->B = 2.4;
agg->C = 1.0;
data->agg = agg;
return 0;
```

Solo los campos `A` y `B` eran de utilidad luego en el cálculo de `ellipse` en la función `sequential`  

### C++

El objetivo de este apartado ha sido paralelizar el código con los threads de C++. Para implementar los threads de C++ a este problema utilice conocimiento previo de la práctica anterior ya que la implementación es bastante similar y lo único que cambia es la operación que se hace con estos threads. Cada thread opera un trozo (lo que en OpenMP se llama chunk size) del problema y esta operación es almacenada en un índice especifico a cada thread dentro de un array. De esta manera me ahorro la implementación de los mutex y también el overhead que estos conllevan.

### OpenMP

El objetivo de este apartado ha sido paralelizar con la API OpenMP. Para llevar a cabo la paralelización del código con el uso de la API OpenMP es necesario levantar threads con el uso del:

`#pragma omp parallel`

y mantener la variable `i` como privada entre los threads. La razón por la cual es necesario llevar a cabo esto es para que cada hilo tenga su propia copia de la variable y realicen las iteraciones que les tocan.

## 3. Metodología y Desarrollo de las Pruebas Realizadas

### Proceso de Benchmarking

Para llevar a cabo los benchmarks se ha ejecutado cada versión del código 10 veces en dos "seasons". Para ésta práctica no se ha desarrollado un script de bash para automatizar las pruebas. Se han ejecutado las pruebas directamente de la consola a través de ejecuciones independientes. Se ha realizado una prueba con antelación a cada benchmark como warm up.

La frecuencia de la CPU se ha fijado con cpufreq. Para averiguar la frecuencia máxima de mi CPU he utilizado lscpu, la cual me indica que la frecuencia máxima es @3100 MHz y luego con el comando top he podido identificar la cantidad de tareas que se estaban corriendo en mi sistema. El número de tareas indicado por el comando top era en promedio 300. Lo único abierto en el ordenador a la hora de ejecutar es la terminal desde la cual se está ejecutando. Debido a que he realizado los benchmarks en un portátil, este se encuentra enchufado a una toma de corriente para que no esté corriendo de batería.

Para obtener los resultados del tiempo de ejecución de la versión secuencial y la versión con threads de C++ se ha utilizado la API gettimeofday. La manera en la que se ha implementado es la siguiente:

```C
struct timeval tv1, tv2;
gettimeofday(&tv1, NULL);
//Código a ejecutar
gettimeofday(&tv2, NULL);
printf ("Total time = %f seconds and the result is %lf\n",
((double) (tv2.tv_usec - tv1.tv_usec) / 1000000 +
(double) (tv2.tv_sec - tv1.tv_sec)), resultado);
```

Para la versión de OpenMP se han utilizado las funciones de OpenMP que permiten obtener el tiempo de la siguiente manera:

```C
double start = omp_get_wtime();

//Proceso que interesa medir

double end = omp_get_wtime() - start;
```

Para decidir qué número de threads funcionaba mejor en mi computadora lleve a cabo una serie de pruebas (2 temporadas con 10 ejecuciones cada una) con diferentes cantidades de threads para el código de C++ con un tamaño de iteraciones de 690000000. Los resultados son los siguientes:

|      | 2 Threads        | 3 Threads        | 4 Threads        | 5 Threads        | 6 Threads        |
|------|------------------|------------------|------------------|------------------|------------------|
| Mean | 5.32430352380952 | 4.94276195238095 | 4.88531933333333 | 4.99209985714286 | 5.01327957142857 |

El "Mean" se refiere a mean execution time en segundos de mis threads.

![](src/imgs/meta-chart.png)

Lo que muestran estos resultados es que el mejor tiempo de ejecución en mi maquina se obtiene al levantar la cantidad de hilos que hay por procesador. Por lo tanto, se aplicará esta política de benchmarking en futuras prácticas para optimizar los resultados de tiempos de ejecución.

### Captura de Resultados

#### Código Secuencial

Resultados del Benchmarking del código secuencial:

| Secuencial | Iteraciones = 10000 | Iteraciones = 690000000 |
|------------|---------------------|-------------------------|
| Mean       | 0.0014591           | 10.38997015             |
| Std. Dev   | 0.000312            | 0.0424225               |

Estos resultados serán utilizados para comparar el rendimiento de los códigos paralelos.

#### Código Paralelo C++

| C++        | Iteraciones = 10000 | Iteraciones = 690000000 |
|------------|---------------------|-------------------------|
| Mean       | 0.001015            | 4.959949                |
| Std. Dev   | 0.000280            | 0.520039                |
| Speedup    | 1.437182            | 2.094773                |
| Eficiencia | 0.359295            | 0.523693                |

![](src/imgs/cpp_small.png)

Grafica que muestra el tiempo de ejecución del código secuencial (azul) y el paralelo (rojo) para un número de iteraciones de 10000. El eje x son las 20 iteraciones que se llevaron a cabo de cada código y el eje y representa los segundos que se ha tardado cada iteración.

![](src/imgs/cpp_big.png)

Grafica que muestra el tiempo de ejecución del código secuencial (azul) y el paralelo (rojo) para un número de iteraciones de 690000000. El eje x son las 20 iteraciones que se llevaron a cabo de cada código y el eje y representa los segundos que se ha tardado cada iteración.

El código paralelo ofrece un speedup significativo ya que aprovecha del reparto del trabajo con el multithreading. Con las gráficas también es interesante apreciar como el código paralelo es capaz de ejecutar el número de iteraciones grande a menos de la mitad del tiempo de ejecución del paralelo. Estos resultados nos permiten concluir que la optimización del software a través del multithreading a sido exitoso.

#### Código Paralelo OpenMP

| OpenMP     | Iteraciónes = 10000 | Iteraciónes = 690000000 |
|------------|---------------------|-------------------------|
| Mean       | 0.002317            | 3.3037254               |
| Std. Dev   | 0.001889            | 0.037949                |
| Speedup    | 0.658641            | 3.144925                |
| Eficiencia | 0.164660            | 0.786231                |

![](src/imgs/omp_small.png)

Grafica que muestra el tiempo de ejecución del código secuencial (azul) y el paralelo en OpenMP (rojo) para un número de iteraciones de 10000. El eje x son las 20 iteraciones que se llevaron a cabo de cada código y el eje y representa los segundos que se ha tardado cada iteración.

![](src/imgs/omp_big.png)

Grafica que muestra el tiempo de ejecución del código secuencial (azul) y el paralelo en OpenMP (rojo) para un número de iteraciones de 690000000. El eje x son las 20 iteraciones que se llevaron a cabo de cada código y el eje y representa los segundos que se ha tardado cada iteración.

Como se puede apreciar por las gráficas y la tabla, el código paralelizado con OpenMP ofrece un speedup considerable e incluso más rápido que el creado con los threads de C++. Sin embargo, es importante notar que para un número de iteraciones bajo es incluso un poco más lento que la versión secuencial y la versión de C++. Esto podría ocurrir debido a que el coste computacional de levantar hilos con la API de OpenMP no compensa para una carga computacional tan baja. Esto es un paradigma importante de la programación paralela, es esencial que el coste computacional de levantar hilos compense. Si este no es el caso es mejor dejar una operación secuencial. Dicho esto, para números de iteraciones grandes si que compensa el uso del paralelismo en OpenMP ya que hasta proporciona una mejora frente a la versión de C++ como se puede ver en los valores del speedup y eficiencia. Para ambas métricas es mejor el código paralelizado en OpenMP para números grandes de iteraciones.  

## 4. Discusión

Esta práctica ha sido la primera en la cual se ha utilizado OpenMP para el paralelismo y ha facilitado el trabajo. Sin embargo, es importante tomar en cuenta que no se han utilizado las directivas de OpenMP que la que permite levantar hilos. Esto ha hecho que el trabajo de paralelizar y repartir la carga computacional entre los hilos sea más complicado, lo cual hace que uno aprecie el nivel de abstracción que OpenMP proporciona ya que esta API permite quitarnos de las manos varios problemas que surgen repetidamente con la paralelización. Ha sido una práctica útil que ha llevado a un mejor manejo del multithreading y programación en C y C++.
