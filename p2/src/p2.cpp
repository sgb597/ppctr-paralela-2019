#include <iostream>
#include <thread>
#include <math.h>
#include <omp.h>
#include <mutex>
#include <sys/time.h>
#include <stdlib.h>

std::mutex mtx;

double ellipse, tmp = 0;
bool op_extra = true;

void sequentialCpp(int posInicial, int numThreads, int trozo, long iteraciones, double *suma);
void sequentialOmp(long numIts, int nthreads);
void sequential(long numIts);

int main(int argc, char* argv[])
{
  int i, numThreads;
  long iteraciones = atol(argv[1]);

  #ifdef SEQ
  struct timeval tv1, tv2;
  gettimeofday(&tv1, NULL);

  sequential(iteraciones);

  gettimeofday(&tv2, NULL);
  printf ("Total time = %f seconds\n",
  (double) (tv2.tv_usec - tv1.tv_usec) / 1000000 +
  (double) (tv2.tv_sec - tv1.tv_sec));
  #endif

  #ifdef CPP

  if (argc == 3)
  {
    numThreads = atoi(argv[2]);
  }
  else
  {
    char* env_p = getenv("threads");
    if (env_p!=NULL){
      numThreads = atoi(env_p);
      printf ("Environment variable is set to %d threads.\n",numThreads);
    }
    else{
      printf("Please specify a number of threads to use\n");
    }
  }

  int trozo = iteraciones/numThreads;
  double suma[numThreads];

  struct timeval tv1, tv2;
  gettimeofday(&tv1, NULL);

  std::thread threads[numThreads];
  for (i=0; i < numThreads; i++){
    threads[i] = std::thread(sequentialCpp, i*trozo, numThreads, trozo, iteraciones, &suma[i]);
  }

  for (i=0; i<numThreads; i++){
    threads[i].join();
    tmp += suma[i];
  }

  gettimeofday(&tv2, NULL);
  printf ("Total time = %f seconds\n",
  (double) (tv2.tv_usec - tv1.tv_usec) / 1000000 +
  (double) (tv2.tv_sec - tv1.tv_sec));
  #endif

  #ifdef OMP

  if (argc == 3)
  {
    numThreads = atoi(argv[2]);
  }
  else
  {
    char* env_p = getenv("OMP_NUM_THREADS");
    if (env_p!=NULL){
      numThreads = atoi(env_p);
      printf ("Environment variable is set to %d threads.\n",numThreads);
    }
    else{
      printf("Please specify a number of threads to use\n");
    }
  }

  double start = omp_get_wtime();

  sequentialOmp(iteraciones, numThreads);

  double end = omp_get_wtime() - start;

  printf("Total time = %f seconds\n", end);
  #endif

  ellipse = (40*tmp)/(iteraciones * sqrt(6));

  printf("ellipse: %lf\n", ellipse);
}

//Funcion para calcular con threads de C++
#ifdef CPP
void sequentialCpp(int posInicial, int numThreads, int trozo, long iteraciones, double *suma){

  double x;
  int posFinal = posInicial+trozo;
  *suma = 0;

  for(int i = posInicial; i < posFinal; i++){
    x = (2*i + 1)/(2*iteraciones);
    *suma = *suma + 1/(1 + x*x);
  }

  if(op_extra && iteraciones % numThreads != 0){

    op_extra = false;

    for(int i = trozo*numThreads; i < iteraciones; i++){
      x = (2*i + 1)/(2*iteraciones);
      *suma = *suma + 1/(1 + x*x);
    }
  }
}
#endif

//Funcion para calcular usando la API OpenMP
#ifdef OMP
void sequentialOmp(long numIts, int nthreads){

  long i;
  double x = 0;
  int posInicial, posFinal;
  int tid = omp_get_thread_num();

  #pragma omp parallel default (shared) private (i, x, tid) reduction(+:tmp)
  {
    posInicial = (numIts/nthreads)*tid;
    posFinal = (numIts/nthreads)*(tid + 1) - 1;

    for (i = posInicial; i < posFinal; i++)
    {
      x = (2*i + 1)/(2*numIts);
      tmp = tmp + 1/(1 + x*x);
    }

    if(op_extra && numIts % nthreads != 0){

      op_extra = false;

      for(i = (nthreads)*(numIts/nthreads); i < numIts; i++)
      {
        x = (2*i + 1)/(2*numIts);
        tmp = tmp + 1/(1 + x*x);
      }
    }
  }
}
#endif

//Funcion para calcular de modo secuencial
#ifdef SEQ
void sequential(long numIts){

  long i;
  long double x;

  for (i = 0; i < numIts; i++)
  {
    x = (2*i + 1)/(2*numIts);
    tmp = tmp + 1/(1 + x*x);
  }
}
#endif
