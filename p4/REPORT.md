# P4: Filtrado de un Video mediante Tareas OpenMP
Sebastián García Bustamante

Fecha 16/12/2019

## Prefacio

En esta práctica se estudiaron e implementaron los conceptos de tasks de OpenMP, compilación condicional, optimización de código y benchmarking.

La práctica me ha parecido un interesante desafío debido a que requiere que el estudiante estudie y se haga aún más familiar con la programación en C, en este caso siendo un caso interesante con un programa que se encarga de filtrar imágenes bidimensionales.

## Índice

1. Sistema
2. Diseño e Implementación del Software
3. Metodología y desarrollo de las pruebas realizadas
4. Discusión

## 1. Sistema

Descripción del sistema donde se realiza el *benchmarking*.


- Memoria de 8GB
- Intel® Core™ i5-7200U CPU @ 2.50GHz
- CPU 4 cores: 1 socket, 2 threads/core, 2 cores/socket.
- Sistema Operativo: Ubuntu 18.04.3 LTS 64-bit
- Intel® HD Graphics 620 (Kaby Lake GT2)
- CPU @3076.257 MHz (fijada con cpufreq)
- Trabajo y benchmarks sobre HDD
- 300 procesos en promedio antes de ejecuciones
- flags: fpu, vme, de, pse, tsc, msr, pae, mce, cx8, apic, sep, mtrr, pge, mca, cmov, sse, sse2, ss y avx

## 2. Diseño e Implementación del Software

### 2.1 Cambios y Modificaciónes

El código generador de imágenes llamado `generator.c` contiene varios casos de código redundante y también realiza funciones que dificultan el filtrado de imágenes que han sido modificadas. Un caso importante que debe de tenerse en cuenta a la hora de utilizar el filtrado de imágenes es que la variable `seq` deberá estar ajustada para filtrar la misma cantidad de imágenes que se crean con la variable `max` desde el generador de imágenes.

Ejemplo de código redundante:

```C
width = rand() % 512 + 1536;
height = rand() % 384 + 1152;
width =  1920;
height = 1440;
```

Adicionalmente se han eliminado los elementos asociados a "META" que son añadidos a la imagen por este fichero. La razón por la cual se ha realizado esta modificación es que dificulta el filtrado de la imagen debido a que la lectura de la imagen es dificultada a la hora que se escriben también líneas asociadas a "META" que no aportan nada a la imagen. La lectura de la imagen producida por `generatoc.c` se hace posible ya que se sabe el tamaño de las imágenes gracias a `width` y `length` (tomando en cuenta los márgenes también). El código que ha sido quitado para realizar el filtrado ha sido:

```C
#define META 128
#define HEADER_META "video custom binary format 0239"

const char* header = HEADER_META;
const char sfooter[] = {'f','r','a','m','e','s',' ','c','h','u','n','k'};
```

```C
int *meta = (int*) malloc(sizeof(int) * META);
memcpy(meta, header, strlen(header)); // rand
fwrite(meta, META * sizeof(int), 1, out);
memset(meta, 0, META);
memcpy(meta, sfooter, strlen(sfooter));
```

Modificación de código sobre la macro "image" ya que no tomaba en cuenta los márgenes de la imagen:

```C
//Código original
#define image(x,y) pixels[x*width+y]

//Código modificado
#define image(x,y) pixels[x*(width+2)+y]
```

### 2.2 Paralelización en OpenMP

Las directivas utilizadas para realizar la optimización del código a través del multithreading con esta API han sido :

- #pragma omp master: Hace que el master thread ejecute lo que se encuentra dentro de las llaves. Similar al #pragma omp single pero la diferencia es que el thread que ejecutara el bloque de código tiene que ser el master thread. Se ha implementado ya que se pide para la práctica.

- #pragma omp task: Las tareas de OpenMP. Son tareas que contienen código a ser ejecutado por threads. Se crean en un instante de tiempo por un thread y se pueden realizar por cualquiera de los threads disponibles. La implementación de esto hace que la paralelización sea asíncrona ya que al crear estas tareas para los demás threads se dejan estas tareas en una cola y no hay ninguna garantía que se vayan a ejecutar en seguida. Las tareas se crean y los threads las ejecutaran cuando puedan sin ninguna garantía de que se realicen a lo más sean creadas.

### 2.3 Variables Etiquetadas

Para la paralelización hecha a través de tasks no se declaran privadas variables al levantar hilos. Esto se debe a que la variable con la cual se itera por los diferentes índices de cada lectura y task a crear es única para el master thread. Esto se debe al uso de la directiva #pragma omp master con la cual forzó a que el master thread sea el que ejecute esa región de código y se dedique a generar tareas para los demás threads de mi región paralela.

```C
#pragma omp parallel num_threads(numt)
{
  #pragma omp master
  {
    while(!feof(in))
    {
      size = fread(pixels[i], (height+2) * (width+2) * sizeof(int), 1, in);

      if(size){

        #pragma omp task
        {
          fgauss(pixels[i], filtered[i], height, width);
          fwrite(filtered[i], (height+2) * (width + 2) * sizeof(int), 1, out);
        }
      }
      i++;
    }
  }
}
```

Para la optimización de mi función fgauss se declaran privadas las siguientes variables cuando levanto hilos:

```C
  #pragma omp parallel default(shared) private(x, y, dx, dy, sum)
```

En este caso son privadas `x`,`y` , `dx`, `dy` ya que son variables con las cuales se está iterando a través de los loops. La razón por la cual necesito que cada thread guarde su propia copia de estas variables es que de caso contrario cada thread modificara la variable con cada iteración y se saltaran índices, lo cual llevaría a tener un proceso mal hecho. `Sum` es privada debido a que cada thread guarda su resultado en esta variable y luego a base de ese resultado se almacena dentro de un array de la siguiente manera:

```C
filtered[x*height+y] = (int) sum/273;
```

Cada thread ha ejecutado y calculado su propia variable privada `sum` que se guarda en el índice único del array que contiene la imagen filtrada dentro del índice que corresponde a ese thread (debido a que cada thread tiene sus copias de las variables `x` e `y`).

### 2.4 Funcionamiento de Paralelismo

El bloque de código paralelizado es un problema de consumidor - productor en el cual el master thread está leyendo del fichero "in" y produciendo tasks para los demás threads. El primer paso es levantar threads con la directiva #pragma omp parallel e indicar que el número de threads a levantar será obtenido de la siguiente manera:

```C
int numt = omp_get_max_threads();

#pragma omp parallel num_threads(numt)
```
De esta manera el código paralelizado se podrá optimizar a cualquier maquina ya que levantara tantos threads como puedan ser indicados con la función de "omp_get_max_threads".

Luego, el master thread entra en un bucle en el cual va a leer del fichero in y producir las tasks de filtrado y escritura para los otros threads siempre y cuando no se haya llegado al final del fichero "in". Se utiliza un contador `i` al cual se le suma 1 después de cada ciclo del while loop para que se lea de fichero y se creen tasks de acuerdo a este índice.

Los threads que están esperando irán tomando tareas de la cola y ejecutándolas hasta que se terminen de la cola.

Al haber vuelto a estudiar esta práctica para el examen me ha surgido una duda al ver mi código original. Para mi propuesta original no utilice el taskwait debido a que el trabajo de leer toma tiempo y en lo que un thread ejecutaba el filtrado y el master thread seguiría leyendo el fichero "in" para generar la task. Lo que significa que el ritmo de leer de fichero seria lo lento suficiente para que por lo menos en mis intentos nunca había tenido problemas de escritura. Sin embargo, me surgió la duda de que podría pasar si dos threads tomaran tasks al mismo tiempo y al acabar escribieran al mismo tiempo. Mi código original no me puede asegurar que esto jamás ocurra, siempre dependo de que el trabajo de leer de fichero sea lento y de que los threads no vayan a tomar las tasks al mismo tiempo (es muy poco probable, pero podría llegar a pasar).

Por lo tanto, he creado la siguiente versión con el uso del taskwait:

```C
double start = omp_get_wtime();
int numProcessors = omp_get_max_threads();
#pragma omp parallel num_threads(numProcessors)
{
  #pragma omp master
  {
    while(!feof(in))
    {
      size = fread(pixels[i], (height+2) * (width+2) * sizeof(int), 1, in);

      if(size){

        #pragma omp task
        {
          fgauss(pixels[i], filtered[i], height, width);
        }
      }
      if (i == seq -1)
      {
        #pragma omp taskwait
        {
          i=0;
            for (int i = 0; i<seq;i++)
            {
              fwrite(filtered[i], (height+2) * (width + 2) * sizeof(int), 1, out);
            }
        }
      }
      else
      {
        i++;
      }
    }
  }
}
```
Con esta versión del código le delego el trabajo de leer, delegar el trabajo de filtrado (tasks) y (al haber acabado el filtrado) la escritura a mi master thread. Este código es el que se puede encontrar en mi repositorio como "video_task2.c".

### 2.5 Calculo de Speedup

El cálculo de speedup, eficiencia, presentación de gráficas y tablas de resultados se realizará en el apartado 3, Captura de Resultados. Las fórmulas que serán utilizadas para ese análisis serán:

![](src/imgs/speedup.png)


![](src/imgs/eficiencia.png)

También se realizará un análisis a través de la media y desviación estándar del tiempo de ejecución del código paralelo y secuencial

### 2.6 Optimización de fgauss

Mi propuesta para la optimización de fgauss es la siguiente:

```C
void fgaussOMP (int *pixels, int *filtered, long height, long width)
{
  int y, x, dx, dy;
  int filter[5][5] = {1, 4, 6, 4, 1, 4, 16, 26, 16, 4, 6, 26, 41, 26, 6, 4, 16, 26, 16, 4, 1, 4, 6, 4, 1};
  int sum;
  int numProcessors = omp_get_max_threads();

  #pragma omp parallel default(shared) private(x, y, dx, dy, sum)
  {
    #pragma omp for schedule(dynamic, numProcessors)
    for (x = 0; x < width; x++)
    {
      for (y = 0; y < height; y++)
      {
        sum = 0;
        for (dx = 0; dx < 5; dx++)
          for (dy = 0; dy < 5; dy++)
            if (((x+dx-2) >= 0) && ((x+dx-2) < width) && ((y+dy-2) >=0) && ((y+dy-2) < height))
              sum = sum + pixels[(x+dx-2),(y+dy-2)] * filter[dx][dy];
        filtered[x*height+y] = (int) sum/273;
      }
    }
  }
}
```

Al paralelizar de esta manera lo que hago es repartir el trabajo de filtrado entre los threads de manera dinámica ya que no tendrán la misma carga todos los hilos. Sin embargo, es esencial notar que esta version de fgauss solo puede utilizarse si no se paraleliza el main. Si se intentase paralelizar el main y también fgauss lo que ocurriría seria "undefined behavior" ya que no se puede garantizar el funcionamiento apropiado de esto. Las diferencias de ganancia obtenidas frente a la paralelización en el main a través de tasks y taskwait serán estudiadas más a fondo en el apartado 3, Captura de Resultados.

## 3. Metodología y Desarrollo de las Pruebas Realizadas

### Proceso de Benchmarking

El proceso de Benchmarking ha sido igual que en la Practica 2. Los parámetros que son únicos para esta práctica y de relevancia son los valores de las variables:

- `seq` y `max` que ambas indican la cantidad de imágenes a procesar por lo tanto estas deberán tener el mismo valor. El valor utilizado para el proceso de benchmarking es 80 para estas dos variables como se indica en el guion de la Practica.

- Tamaño de imagen: `width` =  1920 y `height` = 1440 como se indica en el guion de la Practica.

- Cantidad de hilos utilizados: Obtenido a través de la función omp_get_max_threads para que el rendimiento del código se adapte mejor al computador que se encuentre ejecutándolo. En el caso de mi computador se obtienen 4 hilos con esta función de OpenMP.

### Captura de Resultados

#### Código Secuencial

Resultados del Benchmarking del código secuencial:

| Secuencial | seq = 80  |
|------------|-----------|
| Mean       | 33.511203 |
| Std. Dev.  | 0.214723  |

Estos resultados serán utilizados para comparar el rendimiento de los códigos paralelos.

#### Código Paralelo OpenMP

| OpenMP     | Tasks    | Parallel fgauss |
|------------|----------|-----------------|
| Mean       | 21.74962 | 17.839992       |
| Std. Dev   | 3.373696 | 2.880329        |
| Speedup    | 1.540771 | 1.878431        |
| Eficiencia | 0.385192 | 0.469607        |

La siguiente grafica muestra los tiempos de ejecución para el código secuencial (rojo), código paralelo OpenMP con tasks (azul) y el código ejecutando fgauss paralelizada como se ha mostrado en el apartado 2.6 (amarillo).

![](src/imgs/video.png)

Se ha realizado la comparación con el comando diff para cada una de las "movies" que salen del código y todas son iguales.

Los resultados que se han mostrado revelan algo muy interesante sobre la programación paralela con las directivas que se han utilizado. La razón por la cual la media de tiempo de ejecución es mayor para la paralelización usando tasks es por la naturaleza de las tasks en OpenMP. Un hilo ejecuta un segmento de código crea unas tareas, que luego serás ejecutadas, probablemente por un hilo diferente. El momento exacto de la ejecución de la tarea depende de un planificador de tareas, que opera de manera invisible para el usuario. Las tasks son creadas, puestas en una cola y son realizadas cuando los hilos están libres y las toman. Esto explica porque se obtienen diferentes tiempos de ejecución ya que el código que utiliza las tasks hace uso de la paralelización asíncrona en la cual un suceso no tiene lugar en total correspondencia temporal con otro suceso. Por otra mano, el código que utiliza una ejecución "secuencial" del main pero una función fgauss paralelizada tiene un rendimiento levemente mejor. En fgauss paralela se hace uso de la paralelización síncrona con directivas de OpenMP para repartir el trabajo de filtrado de imagen entre threads.

| OMP Tasks V2 | seq = 80  |
|--------------|-----------|
| Mean         | 28.887723 |
| Std. Dev     | 1.139388  |
| Speedup      | 1.160049  |
| Eficiencia   | 0.290012  |

Como se puede ver al comparar los resultados de esta versión del código con la implementación del taskwait, es considerablemente más lenta que la versión que no usa el taskwait y que la versión que paraleliza el trabajo de fgauss. Sin embargo, con esta versión se asegura el funcionamiento apropiado y que no vaya a haber sobre escritura de ninguna manera.

## 4. Discusión

Esta práctica ha sido una manera muy buena de abordar temas de paralelización ya que ha hecho que se utilice el pensamiento crítico para lograr paralelizar con tasks y como mejorar fgauss a través del paradigma de la programación paralela.
