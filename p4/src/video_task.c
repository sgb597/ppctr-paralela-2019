#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <omp.h>
#define SEQ 1
#define OMP 0
#define FGAUSSOMP 0
#define DEBUG 0


void fgauss (int *, int *, long, long);
void fgaussOMP (int *, int *, long, long);

int main(int argc, char *argv[]) {

  FILE *in;
  FILE *out;
  int i, j, size, seq = 10;
  int **pixels, **filtered;

  //chdir("/tmp");
  in = fopen("movie.in", "rb");
  if (in == NULL) {
    perror("movie.in");
    exit(EXIT_FAILURE);
  }

  #if SEQ == 1
  out = fopen("movie.out", "wb");
  if (out == NULL) {
    perror("movie.out");
    exit(EXIT_FAILURE);
  }
  #endif

  #if OMP == 1
  out = fopen("movieOMP.out", "wb");
  if (out == NULL) {
    perror("movie.out");
    exit(EXIT_FAILURE);
  }
  #endif

  long width, height;

  fread(&width, sizeof(width), 1, in);
  fread(&height, sizeof(height), 1, in);

  fwrite(&width, sizeof(width), 1, out);
  fwrite(&height, sizeof(height), 1, out);

  pixels = (int **) malloc (seq * sizeof (int *));
  filtered = (int **) malloc (seq * sizeof (int *));

  for (i=0; i<seq; i++)
  {
    pixels[i] = (int *) malloc((height+2) * (width+2) * sizeof(int));
    filtered[i] = (int *) malloc((height+2) * (width+2) * sizeof(int));
  }

  #if OMP == 1
  i = 0;

  double start = omp_get_wtime();
  int numt = omp_get_max_threads();

  #pragma omp parallel num_threads(numt)
  {
    #pragma omp master
    {
      while(!feof(in))
      {
        size = fread(pixels[i], (height+2) * (width+2) * sizeof(int), 1, in);

        if(size){

          #pragma omp task
          {
            fgauss(pixels[i], filtered[i], height, width);
            fwrite(filtered[i], (height+2) * (width + 2) * sizeof(int), 1, out);
          }
        }
        i++;
      }
    }
  }
  double time = omp_get_wtime() - start;
  printf("Execution time for the parallel version: %lf seconds\n", time);

  #endif


  #if SEQ == 1
  i = 0;

  double start_seq = omp_get_wtime();
  do
  {
    size = fread(pixels[i], (height+2) * (width+2) * sizeof(int), 1, in);

    if (size)
    {
      fgauss (pixels[i], filtered[i], height, width);
      fwrite(filtered[i], (height+2) * (width + 2) * sizeof(int), 1, out);
    }

    i++;

  } while (!feof(in));

  double time_seq = omp_get_wtime() - start_seq;
  printf("Execution time for the sequential version: %lf seconds\n", time_seq);
  #endif

  #if FGAUSSOMP == 1
  i = 0;

  double start_seq = omp_get_wtime();
  do
  {
    size = fread(pixels[i], (height+2) * (width+2) * sizeof(int), 1, in);

    if (size)
    {
      fgaussOMP (pixels[i], filtered[i], height, width);
      fwrite(filtered[i], (height+2) * (width + 2) * sizeof(int), 1, out);
    }

    i++;

  } while (!feof(in));

  double time_seq = omp_get_wtime() - start_seq;
  printf("Execution time for the sequential version: %lf seconds\n", time_seq);
  #endif

  for (i=0; i<seq; i++)
  {
    free (pixels[i]);
    free (filtered[i]);
  }
  free(pixels);
  free(filtered);

  fclose(out);
  fclose(in);

  return EXIT_SUCCESS;
}


void fgauss (int *pixels, int *filtered, long height, long width)
{
  int y, x, dx, dy;
  int filter[5][5] = {1, 4, 6, 4, 1, 4, 16, 26, 16, 4, 6, 26, 41, 26, 6, 4, 16, 26, 16, 4, 1, 4, 6, 4, 1};
  int sum;

  for (x = 0; x < width; x++) {
    for (y = 0; y < height; y++)
    {
      sum = 0;
      for (dx = 0; dx < 5; dx++)
         for (dy = 0; dy < 5; dy++)
           if (((x+dx-2) >= 0) && ((x+dx-2) < width) && ((y+dy-2) >=0) && ((y+dy-2) < height))
             sum += pixels[(x+dx-2),(y+dy-2)] * filter[dx][dy];

      filtered[x*height+y] = (int) sum/273;
    }
  }
}



void fgaussOMP (int *pixels, int *filtered, long height, long width)
{
  int y, x, dx, dy;
  int filter[5][5] = {1, 4, 6, 4, 1, 4, 16, 26, 16, 4, 6, 26, 41, 26, 6, 4, 16, 26, 16, 4, 1, 4, 6, 4, 1};
  int sum;
  int numProcessors = omp_get_max_threads();

  #pragma omp parallel default(shared) private(x, y, dx, dy, sum)
  {
    #pragma omp for schedule(dynamic, numProcessors)
    for (x = 0; x < width; x++)
    {
      for (y = 0; y < height; y++)
      {
        sum = 0;
        for (dx = 0; dx < 5; dx++)
          for (dy = 0; dy < 5; dy++)
            if (((x+dx-2) >= 0) && ((x+dx-2) < width) && ((y+dy-2) >=0) && ((y+dy-2) < height))
              sum = sum + pixels[(x+dx-2),(y+dy-2)] * filter[dx][dy];
        filtered[x*height+y] = (int) sum/273;
      }
    }
  }
}
