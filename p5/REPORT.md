# P5 Fractales de Mandelbrot
Sebastián García Bustamante

Fecha 20/12/2019

## Prefacio

En esta práctica se estudiaron e implementaron los conceptos de paralelización de OpenMP, compilación condicional, optimización de código y benchmarking.

La práctica me ha parecido un interesante desafío debido a que requiere que el estudiante estudie y se haga aún más familiar con la programación en C, en este caso siendo un caso interesante con un programa que se encarga de filtrar imágenes bidimensionales.

## Índice

1. Sistema
2. Diseño e Implementación del Software
3. Metodología y desarrollo de las pruebas realizadas
4. Discusión

## 1. Sistema

Descripción del sistema donde se realiza el *benchmarking*.


- Memoria de 8GB
- Intel® Core™ i5-7200U CPU @ 2.50GHz
- CPU 4 cores: 1 socket, 2 threads/core, 2 cores/socket.
- Sistema Operativo: Ubuntu 18.04.3 LTS 64-bit
- Intel® HD Graphics 620 (Kaby Lake GT2)
- CPU @3081.823 MHz (fijada con cpufreq)
- Trabajo y benchmarks sobre HDD
- 300 procesos en promedio antes de ejecuciones
- flags: fpu, vme, de, pse, tsc, msr, pae, mce, cx8, apic, sep, mtrr, pge, mca, cmov,   sse, sse2, ss y avx

## 2. Diseño e Implementación del Software

### Ejercicio 1

Para llevar a cabo el profiling se han cambiado los parámetros de `n` y `max_count` para poder analizar qué es lo que ocurre al ejecutar el código de la práctica 5.

Para el profiling original se redujeron ambos valores a 10 y los resultados fueron:

![](src/imgs/smallprof.png)

Luego se cambiaron ambos valores a 500 para verificar si había un cambio en lo que mostraría el kcachegrind.

![](src/imgs/bigprof.png)

Como se puede ver en los resultados del profiling con kcachegrind, la función explode es en la que más coste computacional hay. Una vez se identifica la función que causa el cuello de botella de nuestro programa se necesita saber dónde paralelizar ya que esto podría ocurrir o bien dentro de la función o en el main donde se genera la llamada a esa función. Debido a que se llama a esta función dentro de una región de un for loop con un nested loop donde ambos van del 0 a n-1 se ha decidido paralelizar esa sección de código debido a la gran cantidad de llamadas que se generan a explode dentro de estos.

### Ejercicio 2

#### Directivas OpenMP

Las directivas que se han utilizado son:

- el scheduler dinámico: Me permite repartir el trabajo de manera dinámica entre mis threads en vez de siempre darles un tamaño fijo. He decidido implementar este scheduler ya que los cálculos a realizar por los threads no siempre sera fija, por lo tanto, es importante nivelar el trabajo entre mis threads y que no haya uno que se esté llevando más carga computacional (lo cual haría más lento y disminuiría mi ganancia).

- single: pragma omp single me permite hacer que solo un thread ejecute el bloque de código indicado dentro. Muy parecido al pragma omp máster de la práctica pasada solo que en este caso no importa que thread lo ejecute.

#### Variables Etiquetadas

```C
  #pragma omp parallel default (shared) private(i, j, x, y)
```

Las variables han sido etiquetadas de esta manera al levantar hilos en mi programa. Para indicar que necesito que cada thread mantenga una copia de sus variables lo hago a través del private. Indico que quiero esas variables como privadas ya que de lo contrario tendría a todos los threads cambiando y actualizando al iterar y se terminarían machacando los resultados. Las demás variables que sean utilizadas en mi región paralelas las pongo como compartidas ya que no habrá ningún tipo de condición de carrera sobre ellas o simplemente son como `n` que ayudan a determinar las longitudes de los loops.

#### Funcionamiento del Paralelismo

Al levantar hilos e indicar que el for loop del exterior utilize un reparto dinámico del trabajo reparto a mis threads el trabajo del for loop exterior por partes. Esto significa que cada thread hará un cierto rango de iteraciones del for loop exterior y realizará todas las iteraciones correspondientes para del loop interior. Dentro del segundo loop se encuentran `x` e `y` (que al ser privadas serán únicas para cada thread y no podrán ver la de los demás) sobre las cuales se calcula base de los índices en los que se encuentra el loop. Luego se guarda el resultado de explode dentro del array `count`.

Luego en el código se han probado diferentes alternativas para obtener el resultado de `c_max`. Estas variantes buscan repartir el trabajo de los for loops entre threads.

- Se realiza una implementación mediante directivas de OpenMP en las cuales se utiliza el scheduler "dynamic" para repartir el trabajo de manera dinámica (dependiendo de la carga generada).
- Una implementación con los locks de OpenMP (cuya implementación es similar a los mutex de C++ que hemos practicado).

- Una versión secuencial donde le encargo a un thread cualquiera ejecutar el cálculo entero.

### Ejercicio 3

Para fácil visibilidad se ha incluido un fichero C llamado "c_max.c" en el cual se implementan todas las versiones de este código. Los diferentes tiempos de ejecución de estas implementaciones serán estudiadas más a fondo en el apartado de Captura de Resultados.

## 3. Metodología y Desarrollo de las Pruebas Realizadas

### Proceso de Benchmarking

El proceso de Benchmarking ha sido igual que en la Práctica 2. Los parámetros que son únicos para esta práctica y de relevancia son los valores de las variables:

- `n` que define el tamaño de mi imagen cuadrada de salida. Para el benchmarking se ha puesto a 4000.

- `count_max`que define el tamaño de los loops de la función explode. Limita el número de iteraciones con las cuales se decide si el numero complejo diverge o no de la sucesión. Se ha utilizado un count_max a 1000 para los benchmarks.

- `num_threads` se ha utilizado la función de runtime omp_get_max_threads() que por defecto obtiene el número de cores que tiene la computadora en la cual se está ejecutando. En mi caso son 4 cores y por lo tanto es lo más óptimo para mi computadora.

### Captura de Resultados

#### Código Secuencial

Resultados del Benchmarking del código secuencial:

| Secuencial | Fractal de n = 4000 |
|------------|---------------------|
| Mean       | 16.794229           |
| Std. Dev   | 0.034011            |

Estos resultados serán utilizados para comparar el rendimiento de los códigos paralelos.

#### Código Paralelo OpenMP

| OMP        | Fractal de n = 4000 |
|------------|---------------------|
| Mean       | 8.424831            |
| Std. Dev   | 0.102747            |
| Speedup    | 1.993420            |
| Eficiencia | 0.498355            |

La siguiente grafica muestra los tiempos de ejecución para el código secuencial (rojo), código paralelo OpenMP con tasks (azul). El eje x es el número de iteraciones que se han realizado (10 ejecuciones por temporada).

![](src/imgs/ompvsseq.png)

Se ha realizado la comparación con el comando diff para cada una de los fractales que salen del código y los resultados son iguales.

Al repartir el trabajo entre threads se ve una ganancia del doble. Esto de realiza al repartir el trabajo con un schedule dinámico ya que la carga de trabajo no sera la misma para todas las ejecuciones de los loops (ya que en algunos casos divergen antes y en algunos tienen que llegar hasta `count_max`).

#### `c_max` Directivas de OpenMP

| Ejercicio 1 | Fractal de n = 4000 |
|-------------|---------------------|
| Mean        | 0.016613            |
| Std. Dev    | 0.002135            |
| Speedup     | 2.258802            |
| Eficiencia  | 0.564700            |

#### `c_max` Funciones de runtime

| Ejercicio 1 | Fractal de n = 4000 |
|-------------|---------------------|
| Mean        | 0.017471            |
| Std. Dev    | 0.003589            |
| Speedup     | 2.147844            |
| Eficiencia  | 0.536961            |

#### `c_max` Implementación Secuencial

| Secuencial | Fractal de n = 4000 |
|------------|---------------------|
| Mean       | 0.037526            |
| Std. Dev   | 0.000704            |

Los benchmarks se han ejecutado de la misma manera con los mismos parámetros. En el grafico a continuación se tiene a las tres implementaciones de código y sus tiempos de ejecución. La línea amarilla es el código secuencial, la roja es con las funciones de runtime (lock) y la línea azul es el código con la implementación de directivas de OpenMP.

![](src/imgs/c_max.png)

Como era de esperar, el código secuencial toma más tiempo que los que se reparten el trabajo. La implementación de lock es un poco más lenta probablemente al hecho que se tienen que estar tomando turnos con el lock para poder registrar sus resultados y la implementación de OpenMP da mejores resultados por poco.

## 4. Discusión

Esta práctica ha sido una manera muy buena de abordar temas de paralelización ya que ha hecho que se utilice el pensamiento crítico para entender el problema y también poder analizar por donde se pueden optimizar los códigos con paralelización.
